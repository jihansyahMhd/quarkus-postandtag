package org.acme.exception;

public class ResourceNotFoundException extends Exception {
	
	private static final long serialVersionUID = -6228289942394035340L;

	public ResourceNotFoundException(String msg) {
		super(msg);
	}
}
