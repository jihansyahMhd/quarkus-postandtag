package org.acme.exception;

public class HttpNotFoundException extends Exception {
	
	private static final long serialVersionUID = -3747741727493337278L;
	
	public HttpNotFoundException(String msg) {
		super(msg);
	}
}
