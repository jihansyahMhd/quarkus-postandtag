package org.acme.exception;

public class DuplicateResourceException extends Exception {

	private static final long serialVersionUID = 4796871628900735471L;

	public DuplicateResourceException(String msg) {
		super(msg);
	}
}
