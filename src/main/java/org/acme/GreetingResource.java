package org.acme;

import org.acme.Interface.PostInterface;
import org.acme.Interface.TagInterface;
import org.acme.controller.BaseController;
import org.acme.dto.CommonRs;
import org.acme.dto.PostDto;
import org.acme.dto.TagDto;
import org.acme.entity.Post;
import org.jboss.logging.annotations.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@RestController
@RequestMapping("/hello-resteasy")
public class GreetingResource extends BaseController {



    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        return "Hello RESTEasy";
    }





//



}