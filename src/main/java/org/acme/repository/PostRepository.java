package org.acme.repository;

import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import org.acme.dto.PostDto;
import org.acme.entity.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import java.util.List;

public interface PostRepository extends JpaRepository<Post,String> {

    @Query(value = "SELECT g FROM Post g WHERE UPPER(g.title)=UPPER(:title)")
    List<Post> findAllPostDtoByTitle(@Param("title") String title);
    List<Post>findAllByIdIn(List<String>id);



}
