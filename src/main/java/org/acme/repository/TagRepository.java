package org.acme.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import org.acme.entity.Post;
import org.acme.entity.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;

public interface TagRepository extends JpaRepository<Tag,String> {
    @Query(value = "SELECT g FROM Tag g WHERE UPPER(g.label)=UPPER(:label)")
    List<Tag> findAllTagDtoByLabel(@Param("label") String label);
    List<Tag>findAllByIdIn(List<String>id);
    Tag findByLabel(String label);
}
