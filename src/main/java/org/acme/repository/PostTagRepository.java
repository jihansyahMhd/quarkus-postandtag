package org.acme.repository;

import org.acme.entity.PostTag;
import org.springframework.data.jpa.repository.JpaRepository;


import java.util.List;

public interface PostTagRepository extends JpaRepository<PostTag,String> {
    List<PostTag>findAllByPostId(String postId);
    List<PostTag>findAllByTagId(String tagId);

}
