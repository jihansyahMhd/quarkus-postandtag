package org.acme.service;

import org.acme.Interface.PostInterface;
import org.acme.dto.PostDto;
import org.acme.entity.Post;
import org.acme.exception.DuplicateResourceException;
import org.acme.exception.HttpNotFoundException;
import org.acme.exception.ResourceNotFoundException;
import org.acme.repository.PostRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@ApplicationScoped
public class PostInterfaceImpl implements PostInterface {
    @Inject
    PostRepository postRepository;

    @Override
    @Transactional
    public String setPost(PostDto postDto) throws Exception {
        Post post= new Post( postDto.getTitle(),postDto.getContent());
        List<Post>postDtos=postRepository.findAllPostDtoByTitle(postDto.getTitle());
        if(!postDtos.isEmpty())
            throw new DuplicateResourceException("title has been used");
        postRepository.save(post);
        return post.getId();
    }

    @Override
    public Post getById(String id) {
        return postRepository.findById(id).get();
    }

    @Override
    public List<Post> getAllPost() {
        return postRepository.findAll();
    }

    @Override
    public Post updatePost(String id, PostDto postDto)throws Exception {
        Optional<Post> post=postRepository.findById(id);
        if(post.isPresent()){
            List<Post>postDtos=postRepository.findAllPostDtoByTitle(postDto.getTitle());
            if(!postDtos.isEmpty())
                throw new DuplicateResourceException("title has been used");
            post.get().setTitle(postDto.getTitle());
            post.get().setContent(postDto.getContent());
            postRepository.save(post.get());
            return post.get();
        }else
          throw new ResourceNotFoundException("not found");
    }

    @Override
    public void deletePost(String id) throws Exception {
        Optional<Post> post=postRepository.findById(id);
        if(post.isPresent()) {
            postRepository.deleteById(id);
        }else
            throw new ResourceNotFoundException("not found");
    }
}
