package org.acme.service;

import org.acme.Interface.PostTagInterface;
import org.acme.dto.*;
import org.acme.entity.Post;
import org.acme.entity.PostTag;
import org.acme.entity.Tag;
import org.acme.exception.ResourceNotFoundException;
import org.acme.repository.PostRepository;
import org.acme.repository.PostTagRepository;
import org.acme.repository.TagRepository;
import org.acme.util.ObjectMapperUtils;
import org.modelmapper.ModelMapper;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@ApplicationScoped
public class PostTagInterfaceImpl implements PostTagInterface {
    @Inject
    PostTagRepository postTagRepository;
    @Inject
    PostRepository postRepository;
    @Inject
    TagRepository tagRepository;

    @Inject
    ObjectMapperUtils objectMapperUtils;

    @Override
    public List<PostTag> allPostTag() {
        return postTagRepository.findAll();
    }

    @Override
    public List<BulkCommon> insertData(List<PostTagInsertDto> postTagInsertDtos) throws Exception {
        List<BulkCommon> bulkCommons=new LinkedList<>();
        for (PostTagInsertDto postTagInsertDto:postTagInsertDtos) {
            StringBuilder tempMessage=new StringBuilder();
            PostTag postTag=new PostTag();
            Tag tag=new Tag();
            if(!postRepository.findById(postTagInsertDto.getPostId()).isPresent())
                tempMessage.append("Post Id Not Found");
            postTag.setPostId(postTagInsertDto.getPostId());
            List<Tag>tags=tagRepository.findAllTagDtoByLabel(postTagInsertDto.getLabel());
            if(tags.isEmpty()){
                tag.setLabel(postTagInsertDto.getLabel());
                tagRepository.save(tag);
            }else{
                tag.setId(tags.get(0).getId());
            }
            postTag.setTagId(tag.getId());
            if(tempMessage.toString().isEmpty()){
                try {
                    postTagRepository.save(postTag);
                    bulkCommons.add(new BulkCommon(postTag.getId(),null,postTagInsertDto));
                }catch (Exception e){
                    bulkCommons.add(new BulkCommon(null,e.getMessage(),postTagInsertDto));
                }
            }
            else{
                bulkCommons.add(new BulkCommon(null,tempMessage.toString(),postTagInsertDto));
            }
        }
        return bulkCommons;
    }

    @Override
    public PostTagShowSingleDto singleDto(String id) throws Exception {
        Optional<PostTag> postTag=postTagRepository.findById(id);
        if(!postTag.isPresent())
            throw new ResourceNotFoundException("Post Tag Id Not Found");
        PostTagShowSingleDto postTagShowSingleDto=new PostTagShowSingleDto();
        Optional<Post> post=postRepository.findById(postTag.get().getPostId());
        Optional<Tag> tag=tagRepository.findById(postTag.get().getTagId());
        postTagShowSingleDto.setContent(post.isPresent()?post.get().getContent():"");
        postTagShowSingleDto.setTitle(post.isPresent()?post.get().getTitle():"");
        postTagShowSingleDto.setLabel(tag.isPresent()?tag.get().getLabel():"");
        return postTagShowSingleDto;
    }

    @Override
    public PostTagDtoShowByIdPost showByIdPost(String postId) throws Exception {
        Optional<Post> post=postRepository.findById(postId);
        if(!post.isPresent())
            throw new ResourceNotFoundException("Post Id Not Valid");
        List<PostTag> postTag=postTagRepository.findAllByPostId(postId);
        PostTagDtoShowByIdPost postTags=new PostTagDtoShowByIdPost();
        postTags.setTitle(post.get().getTitle());
        postTags.setContent(post.get().getContent());
        if(postTag.isEmpty())
            throw new ResourceNotFoundException("No PostTags Found by PostId");
        List<PostTagIdWithTagDto> postTagIdWithTagDtos=new LinkedList<>();
        for (PostTag posttag:postTag) {
            PostTagIdWithTagDto postTagIdWithTagDto=new PostTagIdWithTagDto();
            postTagIdWithTagDto.setId(posttag.getId());
            Optional<Tag> tag=tagRepository.findById(posttag.getTagId());
            if(tag.isPresent())
                postTagIdWithTagDto.setTagDto(new TagDto(tag.get().getLabel()));
            else{
                postTagIdWithTagDto.setTagDto(new TagDto());
            }
            postTagIdWithTagDtos.add(postTagIdWithTagDto);
        }
        postTags.setPostTagIdWithTagDtos(postTagIdWithTagDtos);
        return postTags;
    }

    @Override
    public PostTagDtoShoowByIdTag showByIdTag(String tagId) throws Exception {
        List<PostTag>tags=postTagRepository.findAllByTagId(tagId);
        if(tags.isEmpty())
            throw new ResourceNotFoundException("No PostTags Found by TagId");
        Optional<Tag> tag=tagRepository.findById(tagId);
        List<Post>posts=postRepository.findAllByIdIn(tags.stream()
                                                            .map(PostTag::getPostId).collect(Collectors.toList()));
        PostTagDtoShoowByIdTag postTagDtoShoowByIdTag=new PostTagDtoShoowByIdTag();
        postTagDtoShoowByIdTag.setLabel(tag.get().getLabel());
        List<PostDto>postDtos=ObjectMapperUtils.mapAll(posts,PostDto.class);
        postTagDtoShoowByIdTag.setPostDtos(postDtos);
        return postTagDtoShoowByIdTag;
    }

    @Override
    public List<BulkCommon> updatePostTag(List<PostTagUpdateDto>postTagUpdateDtos) throws Exception {
        List<BulkCommon>bulkCommons=new LinkedList<>();
        for (PostTagUpdateDto postTagUpdateDto:postTagUpdateDtos) {
            StringBuilder stringBuilder=new StringBuilder();
            Optional<PostTag> postTag=postTagRepository.findById(postTagUpdateDto.getId());
            if(!postTag.isPresent())
                stringBuilder.append("Post Tag Id not Valid");
            postTag.get().setPostId(postTagUpdateDto.getPostId());
            postTag.get().setTagId(postTagUpdateDto.getTagId());
            if(stringBuilder.toString().isEmpty()){
                try{
                    postTagRepository.save(postTag.get());
                    bulkCommons.add(new BulkCommon(postTag.get().getId(),null,postTagUpdateDto));
                }catch (Exception e){
                    bulkCommons.add(new BulkCommon(null,stringBuilder.toString(),postTagUpdateDto));
                }
            }
            else{
                bulkCommons.add(new BulkCommon(null,stringBuilder.toString(),postTagUpdateDto));
            }

        }

        return bulkCommons;
    }

    @Override
    public void deletePostTag(String id) throws Exception {
        Optional<PostTag> postTag=postTagRepository.findById(id);
        if(!postTag.isPresent())
            throw new ResourceNotFoundException("Post Tag Id not Valid");
        postTagRepository.deleteById(id);
    }
}
