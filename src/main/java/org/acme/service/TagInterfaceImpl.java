package org.acme.service;

import org.acme.Interface.TagInterface;
import org.acme.dto.TagDto;
import org.acme.entity.Post;
import org.acme.entity.Tag;
import org.acme.exception.DuplicateResourceException;
import org.acme.exception.HttpNotFoundException;
import org.acme.exception.ResourceNotFoundException;
import org.acme.repository.TagRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

@ApplicationScoped
public class TagInterfaceImpl implements TagInterface {
    @Inject
    TagRepository tagRepository;
    @Override
    public String setTag(TagDto tagDto) throws Exception {
        Tag tag= new Tag(tagDto.getLabel());
        List<Tag>postDtos=tagRepository.findAllTagDtoByLabel(tagDto.getLabel());
        if(!postDtos.isEmpty())
            throw new DuplicateResourceException("title has been used");
        tagRepository.save(tag);
        return tag.getId();
    }

    @Override
    public Tag getById(String id) {
        return tagRepository.findById(id).get();
    }

    @Override
    public List<Tag> getAllTag() {
         return tagRepository.findAll();
    }

    @Override
    public Tag updateTag(String id, TagDto tagDto) throws Exception {
        Optional<Tag> tag=tagRepository.findById(id);
        if(tag.isPresent()){
            List<Tag>postDtos=tagRepository.findAllTagDtoByLabel(tagDto.getLabel());
            if(!postDtos.isEmpty())
                throw new DuplicateResourceException("title has been used");
            tag.get().setLabel(tagDto.getLabel());
            tagRepository.save(tag.get());
            return tag.get();
        }else
            throw new ResourceNotFoundException("not found");    }

    @Override
    public void deleteTag(String id) throws Exception {
        Optional<Tag> tag=tagRepository.findById(id);
        if(tag.isPresent()) {
            tagRepository.deleteById(id);
        }else
            throw new ResourceNotFoundException("not found");
    }

    @Override
    public String getIdByLabel(String label) throws Exception {
        Tag tag=tagRepository.findByLabel(label);
        if(tag==null)
            throw new ResourceNotFoundException("Not Found");
        return tag.getId();
    }
}
