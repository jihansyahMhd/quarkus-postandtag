package org.acme.controller;


import org.acme.dto.CommonRs;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.core.Response;
@ApplicationScoped
public class BaseController {

    public <T> ResponseEntity<CommonRs<T>> ok(T data) {
        return new ResponseEntity<>(new CommonRs<>(HttpStatus.OK.value(), "success", null, data), HttpStatus.OK);
    }

    public <T> ResponseEntity<CommonRs<T>> ok(T data, String msg) {
        return new ResponseEntity<>(new CommonRs<>(HttpStatus.OK.value(), msg, null, data), HttpStatus.OK);
    }

    public <T> ResponseEntity<CommonRs<T>> ok(T data, Long count) {
        return new ResponseEntity<>(new CommonRs<>(HttpStatus.OK.value(), "success", count, data), HttpStatus.OK);
    }

    public <T> ResponseEntity<CommonRs<T>> ok() {
        return new ResponseEntity<>(new CommonRs<>(HttpStatus.OK.value(), "success"), HttpStatus.OK);
    }

    public <T> ResponseEntity<CommonRs<T>> badRequest(T data) {
        return new ResponseEntity<>(new CommonRs<>(HttpStatus.BAD_REQUEST.value(), "bad request", null, data), HttpStatus.BAD_REQUEST);
    }

    public <T> ResponseEntity<CommonRs<T>> badRequest(String msg) {
        return new ResponseEntity<>(new CommonRs<>(HttpStatus.BAD_REQUEST.value(), msg), HttpStatus.BAD_REQUEST);
    }

    public <T> ResponseEntity<CommonRs<T>> created(T data) {
        return new ResponseEntity<>(new CommonRs<>(HttpStatus.CREATED.value(), "success", null, data), HttpStatus.CREATED);
    }
}
