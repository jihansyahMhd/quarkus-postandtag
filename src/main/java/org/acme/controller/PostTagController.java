package org.acme.controller;

import org.acme.Interface.PostTagInterface;
import org.acme.dto.PostTagInsertDto;
import org.acme.dto.PostTagUpdateDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.List;

@RestController
@RequestMapping("/PostTag")
public class PostTagController extends BaseController {
    @Inject
    PostTagInterface postTagInterface;

    @PostMapping(value = "")
    public ResponseEntity<?> savePostTag(@RequestBody List<PostTagInsertDto> postTagInsertDto)throws Exception{
         return ok(postTagInterface.insertData(postTagInsertDto));

    }
    @GetMapping(value = "")
    public ResponseEntity<?>getAllPostTag()throws Exception{
        try {
            return ok(postTagInterface.allPostTag());
        }catch (Exception e){
            return badRequest(e.getMessage());
        }
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<?>getAllPostTag(@PathVariable(name = "id") String id)throws Exception{
        try {
            return ok(postTagInterface.singleDto(id));
        }catch (Exception e){
            return badRequest(e.getMessage());
        }
    }

    @GetMapping(value = "/postId/{postId}")
    public ResponseEntity<?>getAllPostTagByPostId(@PathVariable(name = "postId") String postId)throws Exception{
        try {
            return ok(postTagInterface.showByIdPost(postId));
        }catch (Exception e){
            return badRequest(e.getMessage());
        }
    }

    @GetMapping(value = "/tagId/{tagId`}")
    public ResponseEntity<?>getAllPostTagByTagId(@PathVariable(name = "tagId") String tagId)throws Exception{
        try {
            return ok(postTagInterface.showByIdTag(tagId));
        }catch (Exception e){
            return badRequest(e.getMessage());
        }
    }

    @PutMapping(value = "")
    public ResponseEntity<?>updatePostTag(@RequestBody List<PostTagUpdateDto> postTagUpdateDto)throws Exception{
        try {
            return ok(postTagInterface.updatePostTag(postTagUpdateDto));
        }catch (Exception e){
            return badRequest(e.getMessage());
        }
    }

    @DeleteMapping(value = "")
    public ResponseEntity<?>deletePostTag(@RequestBody String id)throws Exception{
        try {
            postTagInterface.deletePostTag(id);
            return ok();
        }catch (Exception e){
            return badRequest(e.getMessage());
        }
    }




    }
