package org.acme.controller;

import org.acme.Interface.TagInterface;
import org.acme.dto.TagDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

@RestController
@RequestMapping("/Tag")
public class TagController extends BaseController {
    @Inject
    TagInterface tagInterface;

    @PostMapping(value = "")
    public ResponseEntity<?> saveTag(@RequestBody TagDto tag){
        try{
            return ok(tagInterface.setTag(tag));
        }catch (Exception e){
            return badRequest(e.getMessage());
        }
    }
    @GetMapping("/{id}")
    public ResponseEntity<?> getTagById(@PathVariable(name = "id")String id){
        try {
            return ok(tagInterface.getById(id));
        }
        catch (Exception e){
            return badRequest(e.getMessage());
        }
    }

    @GetMapping("/label/{label}")
    public ResponseEntity<?> getTagByLabel(@PathVariable(name = "label")String label){
        try {
            return ok(tagInterface.getIdByLabel(label));
        }
        catch (Exception e){
            return badRequest(e.getMessage());
        }
    }


    @GetMapping(value = "")
    public ResponseEntity<?> getTag(){
        try {
            return ok(tagInterface.getAllTag(), (long) tagInterface.getAllTag().size());
        }
        catch (Exception e){
            return badRequest(e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateTag(@PathVariable(name = "id",required = true)String id,TagDto dtoToUpdate) throws Exception {
        try {
            return ok(tagInterface.updateTag(id,dtoToUpdate));
        }
        catch (Exception e){
            return badRequest(e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteTag(@PathVariable(name = "id",required = true)String id) throws Exception {
        try{
            tagInterface.deleteTag(id);
            return ok();
        }catch (Exception e){
            return badRequest(e.getMessage());
        }
    }
}
