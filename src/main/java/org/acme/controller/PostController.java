package org.acme.controller;

import org.acme.Interface.PostInterface;
import org.acme.dto.PostDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

@RestController
@RequestMapping("/Post")
public class PostController extends BaseController{
    @Inject
    PostInterface postInterface;

    @PostMapping(value = "")
    public ResponseEntity<?> savePost(@RequestBody PostDto post){
        try{
            return ok(postInterface.setPost(post));
        }catch (Exception e){
            return badRequest(e.getMessage());
        }
    }
    @GetMapping("/{id}")
    public ResponseEntity<?> getPostById(@PathVariable(name = "id")String id){
        try {
            return ok(postInterface.getById(id));
        }
        catch (Exception e){
            return badRequest(e.getMessage());
        }
    }


    @GetMapping(value = "")
    public ResponseEntity<?> getPost(){
        try {
            return ok(postInterface.getAllPost(), (long) postInterface.getAllPost().size());
        }
        catch (Exception e){
            return badRequest(e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updatePost(@PathVariable(name = "id",required = true)String id,PostDto dtoToUpdate) throws Exception {
        try {
            return ok(postInterface.updatePost(id,dtoToUpdate));
        }
        catch (Exception e){
            return badRequest(e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deletePost(@PathVariable(name = "id",required = true)String id) throws Exception {
        try{
            postInterface.deletePost(id);
            return ok();
        }catch (Exception e){
            return badRequest(e.getMessage());
        }
    }
}
