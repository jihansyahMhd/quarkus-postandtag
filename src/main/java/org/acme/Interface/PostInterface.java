package org.acme.Interface;

import org.acme.dto.PostDto;
import org.acme.entity.Post;

import java.util.List;

public interface PostInterface {
    String setPost(PostDto postDto) throws Exception;
    Post getById(String id);
    List<Post> getAllPost();
    Post updatePost(String id, PostDto postDto)throws Exception;
    void deletePost(String id) throws Exception;
}
