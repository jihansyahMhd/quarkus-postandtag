package org.acme.Interface;

import org.acme.dto.*;
import org.acme.entity.PostTag;

import java.util.List;

public interface PostTagInterface {
    List<PostTag> allPostTag();
    List<BulkCommon> insertData(List<PostTagInsertDto> postTagInsertDtos)throws Exception;
    PostTagShowSingleDto singleDto(String id)throws Exception;
    PostTagDtoShowByIdPost showByIdPost(String postId)throws Exception;
    PostTagDtoShoowByIdTag showByIdTag(String tagId)throws Exception;
    List<BulkCommon> updatePostTag(List<PostTagUpdateDto> postTagUpdateDtos)throws Exception;
    void deletePostTag(String id)throws Exception;
}
