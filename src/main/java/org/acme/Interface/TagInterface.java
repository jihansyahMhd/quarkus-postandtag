package org.acme.Interface;

import org.acme.dto.TagDto;
import org.acme.entity.Post;
import org.acme.entity.Tag;

import java.util.List;

public interface TagInterface {
    String setTag(TagDto tagDto) throws Exception;
    Tag getById(String id);
    List<Tag> getAllTag();
    Tag updateTag(String id, TagDto tagDto)throws Exception;
    void deleteTag(String id) throws Exception;
    String getIdByLabel(String label)throws Exception;
}
