package org.acme.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "tableTag", indexes = {
        @Index(name = "tableTag_id_idx",  columnList="id")
})
public class Tag {
    @Id
    @Column(name = "id", length = 36)
    @GeneratedValue(generator = "uid")
    @GenericGenerator(name = "uid", strategy = "uuid2")
    private String id;
    @Column(nullable = false)
    private String label;

    public Tag(String id, String label) {
        this.id = id;
        this.label = label;
    }


    public Tag() {
    }

    public Tag(String label) {
        this.label = label;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
