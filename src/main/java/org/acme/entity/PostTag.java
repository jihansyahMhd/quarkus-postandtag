package org.acme.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "tablePostTag", indexes =
        {
        @Index(name = "tablePostTag_id_idx",  columnList="id")
        },
        uniqueConstraints = {
        @UniqueConstraint(columnNames = {"postId", "tagId"})
        }
)
public class PostTag {
    @Id
    @Column(name = "id", length = 36)
    @GeneratedValue(generator = "uid")
    @GenericGenerator(name = "uid", strategy = "uuid2")
    private String id;

    private String postId;
    private String tagId;

    public PostTag(String postId, String tagId) {
        this.postId = postId;
        this.tagId = tagId;
    }

    public PostTag(String id, String postId, String tagId) {
        this.id = id;
        this.postId = postId;
        this.tagId = tagId;
    }

    public PostTag() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getTagId() {
        return tagId;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }
}
