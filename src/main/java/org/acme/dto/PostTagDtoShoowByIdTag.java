package org.acme.dto;

import javax.validation.constraints.NotNull;
import java.util.List;

public class PostTagDtoShoowByIdTag {
    private String label;
    private List<PostDto> postDtos;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<PostDto> getPostDtos() {
        return postDtos;
    }

    public void setPostDtos(List<PostDto> postDtos) {
        this.postDtos = postDtos;
    }

    public PostTagDtoShoowByIdTag(String label, List<PostDto> postDtos) {
        this.label = label;
        this.postDtos = postDtos;
    }

    public PostTagDtoShoowByIdTag() {
    }
}
