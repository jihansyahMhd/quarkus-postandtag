package org.acme.dto;

public class BulkCommon {
    private String id;
    private String message;
    private Object dataRequest;

    public BulkCommon(String id, String message, Object dataRequest) {
        this.id = id;
        this.message = message;
        this.dataRequest = dataRequest;
    }

    public BulkCommon(String id, Object dataRequest) {
        this.id = id;
        this.dataRequest = dataRequest;
    }

    public BulkCommon() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getDataRequest() {
        return dataRequest;
    }

    public void setDataRequest(Object dataRequest) {
        this.dataRequest = dataRequest;
    }
}
