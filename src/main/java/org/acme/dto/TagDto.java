package org.acme.dto;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;


public class TagDto {

    private String label;

    public TagDto(String id, String label) {
        this.label = label;
    }

    public TagDto() {
    }

    public TagDto(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
