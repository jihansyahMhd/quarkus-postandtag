package org.acme.dto;

import java.util.List;

public class PostTagDtoShowByIdPost {
    private String title;
    private String content;
    List<PostTagIdWithTagDto> postTagIdWithTagDtos;


    public PostTagDtoShowByIdPost(String title, String content, List<PostTagIdWithTagDto> postTagIdWithTagDtos) {
        this.title = title;
        this.content = content;
        this.postTagIdWithTagDtos = postTagIdWithTagDtos;
    }

    public PostTagDtoShowByIdPost() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<PostTagIdWithTagDto> getPostTagIdWithTagDtos() {
        return postTagIdWithTagDtos;
    }

    public void setPostTagIdWithTagDtos(List<PostTagIdWithTagDto> postTagIdWithTagDtos) {
        this.postTagIdWithTagDtos = postTagIdWithTagDtos;
    }
}

