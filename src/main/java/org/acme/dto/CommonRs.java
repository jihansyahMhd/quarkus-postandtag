package org.acme.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;


public class CommonRs<T> implements Serializable {
 
    @JsonInclude(JsonInclude.Include.NON_NULL)
    protected int status;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    protected String message;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    protected Long total;
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    protected T data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public CommonRs(int status, String message) {
            
        super();
        this.status = status;
        this.message = message;
        this.total = null;
    }

    public CommonRs(int status, String message, Long total, T data) {
        this.status = status;
        this.message = message;
        this.total = total;
        this.data = data;
    }
}
