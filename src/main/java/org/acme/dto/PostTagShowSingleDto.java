package org.acme.dto;

public class PostTagShowSingleDto {
    private String title;
    private String content;
    private String label;

    public PostTagShowSingleDto() {
    }

    public PostTagShowSingleDto(String title, String content, String label) {
        this.title = title;
        this.content = content;
        this.label = label;
    }



    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }


}
