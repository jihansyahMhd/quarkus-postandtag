package org.acme.dto;

public class PostTagIdWithTagDto {
    private String id;
    private TagDto tagDto;

    public PostTagIdWithTagDto(String id, TagDto tagDto) {
        this.id = id;
        this.tagDto = tagDto;
    }

    public PostTagIdWithTagDto() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public TagDto getTagDto() {
        return tagDto;
    }

    public void setTagDto(TagDto tagDto) {
        this.tagDto = tagDto;
    }
}
