package org.acme.dto;

public class PostDto {
    private String title;
    private String content;

    public PostDto() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public PostDto(String title, String content) {
        this.title = title;
        this.content = content;
    }
}
