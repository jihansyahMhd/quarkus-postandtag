package org.acme.dto;

public class PostTagUpdateDto {
    private String id;
    private String postId;
    private String tagId;

    public PostTagUpdateDto(String id, String postId, String tagId) {
        this.id = id;
        this.postId = postId;
        this.tagId = tagId;
    }

    public PostTagUpdateDto() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getTagId() {
        return tagId;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }
}
