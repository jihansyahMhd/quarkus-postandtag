package org.acme.dto;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

public class PostTagInsertDto {
    private String postId;
    private String label;

    public String getPostId() {
        return postId;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }


    public PostTagInsertDto(String postId, String label) {
        this.postId = postId;
        this.label=label;
    }

    public PostTagInsertDto() {
    }
}
